"""Test gather_classify_tabulate_flow.py."""
import pytest

from phiphi.pipeline_jobs.classify import keyword_match_classifier as kms


@pytest.mark.parametrize(
    "raw_must, expected",
    [
        (
            "apples bananas oranges",
            [
                kms.KeywordMatchRegexConfig(
                    "apples",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}apples{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
                kms.KeywordMatchRegexConfig(
                    "bananas",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}bananas{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
                kms.KeywordMatchRegexConfig(
                    "oranges",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}oranges{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
            ],
        ),
        (
            'apples "bananas oranges"',
            [
                kms.KeywordMatchRegexConfig(
                    # Written regex rather than using f-string to make sure that it is correct
                    "apples",
                    "(?is)(^|[^\\p{L}0-9_])apples([^\\p{L}0-9_]|$)",
                    False,
                ),
                kms.KeywordMatchRegexConfig(
                    # Written regex rather than using f-string to make sure that it is correct
                    "bananas oranges",
                    # The `re` module escapes spaces in user input, though unnecessary, as a side
                    # effect of escaping special characters.
                    "(?is)(^|[^\\p{L}0-9_])bananas\\ oranges([^\\p{L}0-9_]|$)",
                    True,
                ),
            ],
        ),
        (
            '"apples bananas" oranges',
            [
                kms.KeywordMatchRegexConfig(
                    "apples bananas",
                    # The `re` module escapes spaces in user input, though unnecessary, as a side
                    # effect of escaping special characters.
                    (
                        f"(?is){kms.PRE_WORD_BOUNDARY_STR}"
                        "apples\\ bananas"
                        f"{kms.POST_WORD_BOUNDARY_STR}"
                    ),
                    True,
                ),
                kms.KeywordMatchRegexConfig(
                    "oranges",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}oranges{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
            ],
        ),
        (
            # Non latin
            # fmt: off
            '"🍎" 🍌 🍊',
            # fmt: on
            [
                kms.KeywordMatchRegexConfig(
                    "🍎", f"(?is){kms.PRE_WORD_BOUNDARY_STR}🍎{kms.POST_WORD_BOUNDARY_STR}", True
                ),
                kms.KeywordMatchRegexConfig(
                    "🍌", f"(?is){kms.PRE_WORD_BOUNDARY_STR}🍌{kms.POST_WORD_BOUNDARY_STR}", False
                ),
                kms.KeywordMatchRegexConfig(
                    "🍊", f"(?is){kms.PRE_WORD_BOUNDARY_STR}🍊{kms.POST_WORD_BOUNDARY_STR}", False
                ),
            ],
        ),
        (
            # Arabic
            "تفاح موز برتقال",
            [
                kms.KeywordMatchRegexConfig(
                    "تفاح",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}تفاح{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
                kms.KeywordMatchRegexConfig(
                    "موز",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}موز{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
                kms.KeywordMatchRegexConfig(
                    "برتقال",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}برتقال{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
            ],
        ),
        (
            # Arabic with quotes
            # fmt: off
            '"تفاح موز" برتقال',
            # fmt: on
            [
                kms.KeywordMatchRegexConfig(
                    "تفاح موز",
                    # The `re` module escapes spaces in user input, though unnecessary, as a side
                    # effect of escaping special characters.
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}تفاح\\ موز{kms.POST_WORD_BOUNDARY_STR}",
                    True,
                ),
                kms.KeywordMatchRegexConfig(
                    "برتقال",
                    f"(?is){kms.PRE_WORD_BOUNDARY_STR}برتقال{kms.POST_WORD_BOUNDARY_STR}",
                    False,
                ),
            ],
        ),
    ],
)
def test_create_regex_config(raw_must, expected):
    """Test create_regex_config."""
    assert kms.create_keyword_match_regex_config(raw_must) == expected
