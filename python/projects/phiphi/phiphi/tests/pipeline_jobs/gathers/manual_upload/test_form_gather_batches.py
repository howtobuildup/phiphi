"""Unit tests for form_gather_batches function.

Tests cover various scenarios for the form_gather_batches functionality.
"""
import json

import pandas as pd
import pandera as pa
import pytest

from phiphi.api.projects.gathers import schemas
from phiphi.pipeline_jobs.gathers import manual_upload
from phiphi.tests.pipeline_jobs.gathers.manual_upload import conftest


def test_form_gather_batches_basic_functionality():
    """Test basic functionality of form_gather_batches using schema example."""
    # Generate example data using the schema
    input_df = conftest.create_mock_manual_upload_df(1)

    # Call the function
    gather_batches_df = manual_upload.form_gather_batches(
        manual_upload_df=input_df, gather_id=1, job_run_id=100, batch_offset=0, batch_size=1
    )

    # Assertions
    assert isinstance(gather_batches_df, pd.DataFrame)
    assert len(gather_batches_df) == 1
    assert gather_batches_df.iloc[0]["gather_id"] == 1
    assert gather_batches_df.iloc[0]["job_run_id"] == 100
    assert gather_batches_df.iloc[0]["gather_type"] == schemas.ChildTypeName.manual_upload.value
    assert gather_batches_df.iloc[0]["batch_id"] == 0
    assert "json_data" in gather_batches_df.columns
    assert gather_batches_df.iloc[0]["last_processed_at"] is pd.NaT


def test_form_gather_batches_json_content():
    """Test that JSON data is correctly formed in gather batches."""
    # Generate example data using the schema
    input_df = conftest.create_mock_manual_upload_df(1)

    # Call the function
    gather_batches_df = manual_upload.form_gather_batches(
        manual_upload_df=input_df, gather_id=5, job_run_id=500, batch_offset=0, batch_size=1
    )

    # Parse JSON data and verify content
    json_data = json.loads(gather_batches_df.iloc[0]["json_data"])

    assert len(json_data) == 1

    # Verify that the JSON data contains all expected columns from the schema
    expected_columns = list(manual_upload.manual_upload_schema.columns.keys())
    for col in expected_columns:
        assert col in json_data[0], f"Column {col} missing from JSON data"

    input_df_from_json = pd.DataFrame(json_data)
    # Have to massage the datetime
    input_df_from_json["message_datetime"] = input_df_from_json["message_datetime"].astype(
        "datetime64[ms, UTC]"
    )  # type: ignore[call-overload]
    input_df_from_json["tiktok_post_plays"] = input_df_from_json["tiktok_post_plays"].astype(
        "Int64"
    )
    assert input_df_from_json.equals(input_df)


def test_form_gather_batches_empty_input():
    """Test form_gather_batches with an empty DataFrame."""
    input_df = pd.DataFrame(columns=manual_upload.manual_upload_schema.columns.keys())  # type: ignore[call-overload]

    with pytest.raises(pa.errors.SchemaError):
        manual_upload.form_gather_batches(
            manual_upload_df=input_df, gather_id=3, job_run_id=300, batch_offset=0, batch_size=1
        )


def test_form_gather_batches_schema_validation():
    """Test schema validation in form_gather_batches with minimal data."""
    # Attempt to create a DataFrame that will fail schema validation
    with pytest.raises(pa.errors.SchemaError):
        # Intentionally create an incomplete DataFrame
        incomplete_df = pd.DataFrame(
            [
                {
                    # Intentionally missing many required fields
                    "platform": "twitter",
                }
            ]
        )

        manual_upload.form_gather_batches(
            manual_upload_df=incomplete_df,
            gather_id=4,
            job_run_id=400,
            batch_offset=0,
            batch_size=1,
        )


def test_form_gather_batches_multiple_batches():
    """Test form_gather_batches with multiple batches using schema example."""
    # Generate example data using the schema
    input_df = conftest.create_mock_manual_upload_df(5)

    # Call the function with batch size of 2
    gather_batches_df = manual_upload.form_gather_batches(
        manual_upload_df=input_df, gather_id=2, job_run_id=200, batch_offset=10, batch_size=2
    )

    # Assertions
    assert len(gather_batches_df) == 3  # 5 items with batch size 2
    assert list(gather_batches_df["batch_id"]) == [10, 11, 12]
    assert all(batch["gather_id"] == 2 for _, batch in gather_batches_df.iterrows())
    assert all(batch["job_run_id"] == 200 for _, batch in gather_batches_df.iterrows())
    last_json_object = json.loads(gather_batches_df[-1:].iloc[0]["json_data"])
    assert len(last_json_object) == 1  # Last batch has 1 item


def test_form_gather_batches_batch_sizes():
    """Test form_gather_batches with various batch sizes."""
    # Test configurations
    test_configs = [
        {"num_rows": 10, "batch_size": 3},
        {"num_rows": 7, "batch_size": 5},
        {"num_rows": 15, "batch_size": 10},
    ]

    for config in test_configs:
        # Generate example data using the schema
        input_df = conftest.create_mock_manual_upload_df(config["num_rows"])

        # Call the function
        gather_batches_df = manual_upload.form_gather_batches(
            manual_upload_df=input_df,
            gather_id=6,
            job_run_id=600,
            batch_offset=0,
            batch_size=config["batch_size"],
        )

        # Calculate expected number of batches
        expected_batch_count = (config["num_rows"] + config["batch_size"] - 1) // config[
            "batch_size"
        ]

        # Assertions
        assert (
            len(gather_batches_df) == expected_batch_count
        ), f"Incorrect batch count for {config}"

        # Verify that each batch has the correct number of records (except possibly the last)
        for i, batch in enumerate(gather_batches_df.itertuples()):
            json_data = json.loads(batch.json_data)  # type: ignore[arg-type]
            max_expected_records = config["batch_size"]

            assert len(json_data) <= max_expected_records, f"Batch {i} exceeds max batch size"
