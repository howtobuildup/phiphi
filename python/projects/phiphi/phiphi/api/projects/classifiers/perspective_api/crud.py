"""Perspective API crud functionality."""
import sqlalchemy as sa

from phiphi.api import exceptions
from phiphi.api.projects import classifiers
from phiphi.api.projects.classifiers.perspective_api import schemas
from phiphi.api.projects.job_runs import crud as job_run_crud
from phiphi.api.projects.job_runs import schemas as job_run_schemas


async def create_classifier_and_run(
    session: sa.orm.Session,
    project_id: int,
    classifier_create: schemas.PerspectiveAPIClassifierCreate,
) -> schemas.PerspectiveAPIClassifierDetail:
    """Create a perspective API classifier."""
    orm_classifier = create_and_version_orm(session, project_id, classifier_create)
    _ = await job_run_crud.create_and_run_job_run(
        session,
        project_id,
        job_run_schemas.JobRunCreate(
            foreign_id=orm_classifier.id,
            foreign_job_type=job_run_schemas.ForeignJobType.classify_tabulate,
        ),
    )
    session.refresh(orm_classifier)
    return schemas.PerspectiveAPIClassifierDetail.model_validate(orm_classifier)


def create_and_version_orm(
    session: sa.orm.Session,
    project_id: int,
    classifier_create: schemas.PerspectiveAPIClassifierCreate,
) -> classifiers.models.Classifiers:
    """Create a perspective API classifier and version."""
    orm_classifier = classifiers.models.Classifiers(
        project_id=project_id,
        name=classifier_create.name,
        description=classifier_create.description,
        type=classifiers.base_schemas.ClassifierType.perspective_api,
        archived_at=None,
    )
    session.add(orm_classifier)
    session.commit()
    session.refresh(orm_classifier)

    _ = create_version(session, orm_classifier, classifier_create.latest_version)
    session.refresh(orm_classifier)
    return orm_classifier


def create_and_version(
    session: sa.orm.Session,
    project_id: int,
    classifier_create: schemas.PerspectiveAPIClassifierCreate,
) -> schemas.PerspectiveAPIClassifierDetail:
    """Create a perspective API classifier and version."""
    orm_classifier = create_and_version_orm(session, project_id, classifier_create)
    return schemas.PerspectiveAPIClassifierDetail.model_validate(orm_classifier)


def create_version(
    session: sa.orm.Session,
    orm_classifier: classifiers.models.Classifiers,
    classifier_version: schemas.PerspectiveAPIVersionBase,
) -> classifiers.models.ClassifierVersions:
    """Create a perspective API version."""
    dict_version = classifier_version.model_dump()
    orm_version = classifiers.models.ClassifierVersions(
        classifier_id=orm_classifier.id,
        classes=dict_version["classes"],
        params=dict_version["params"],
    )
    session.add(orm_version)
    session.commit()
    session.refresh(orm_version)
    return orm_version


async def create_version_and_run(
    session: sa.orm.Session,
    project_id: int,
    classifier_id: int,
    classifier_version: schemas.PerspectiveAPIVersionBase,
) -> schemas.PerspectiveAPIClassifierDetail:
    """Create a perspective API version and run."""
    orm_classifier = classifiers.crud.get_orm_classifier(session, project_id, classifier_id)
    if orm_classifier is None:
        raise exceptions.ClassifierNotFound()
    _ = create_version(session, orm_classifier, classifier_version)
    _ = await job_run_crud.create_and_run_job_run(
        session,
        project_id,
        job_run_schemas.JobRunCreate(
            foreign_id=orm_classifier.id,
            foreign_job_type=job_run_schemas.ForeignJobType.classify_tabulate,
        ),
    )
    session.refresh(orm_classifier)
    return schemas.PerspectiveAPIClassifierDetail.model_validate(orm_classifier)
