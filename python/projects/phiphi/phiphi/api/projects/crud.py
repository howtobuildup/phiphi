"""Project crud functionality."""
import datetime
import logging

import sqlalchemy.orm

from phiphi import config, utils
from phiphi.api import exceptions
from phiphi.api.projects import credit_allocations, models, schemas, user_project_associations
from phiphi.api.workspaces import models as workspace_models
from phiphi.pipeline_jobs import projects

logger = logging.getLogger(__name__)


def create_orm_project_not_commited(
    session: sqlalchemy.orm.Session,
    user_id: int,
    project: schemas.ProjectCreate,
) -> models.Project:
    """Create a new project without commit.

    This is so it is possible to rollback the transaction if any error occurs in the caller of this
    function.
    """
    orm_workspace = (
        session.query(workspace_models.Workspace)
        .filter(workspace_models.Workspace.slug == project.workspace_slug)
        .first()
    )

    if orm_workspace is None:
        raise exceptions.WorkspaceNotFound()

    try:
        project_base_fields = set(schemas.ProjectBase.model_fields.keys())
        project_data = project.dict(include=project_base_fields)
        orm_project = models.Project(**project_data)
        session.add(orm_project)
        # Get the id of the project without committing the transaction
        session.flush()
        if project.initial_credit_allocation_amount:
            orm_credit_allocations = credit_allocations.CreditAllocations(
                user_id=user_id,
                project_id=orm_project.id,
                amount=project.initial_credit_allocation_amount,
                description=project.initial_credit_allocation_description,
            )
            session.add(orm_credit_allocations)

        session.refresh(orm_project)
        return orm_project
    except Exception as e:
        session.rollback()
        raise e


def create_project(
    session: sqlalchemy.orm.Session,
    user_id: int,
    project: schemas.ProjectCreate,
) -> schemas.ProjectResponse:
    """Create a new project.

    This function can be used to create a new project without external resources, such as for
    tests.
    """
    orm_project = create_orm_project_not_commited(session, user_id, project)
    session.commit()
    session.refresh(orm_project)
    return schemas.ProjectResponse.model_validate(orm_project)


async def create_project_with_external_resources(
    session: sqlalchemy.orm.Session,
    user_id: int,
    project: schemas.ProjectCreate,
) -> schemas.ProjectResponse:
    """Create a new project with external resources."""
    orm_project = create_orm_project_not_commited(session, user_id, project)
    try:
        if not config.settings.USE_MOCK_BQ:
            logger.info(f"Creating big query dataset for project {orm_project.id}")
            project_namespace = utils.get_project_namespace(orm_project.id)
            # Creating with dummy rows as it is easy to test the dashboard
            # Ignoring the type as mypy doesn't know about the async magic of prefect :(
            await projects.init_project_db(  # type: ignore[misc]
                project_namespace, orm_project.workspace_slug, with_dummy_data=True
            )

        if config.settings.ADD_BIG_QUERY_RATE_LIMITS_ON_PROJECT_CREATION:
            logger.info(f"Adding big query rate limits for project {orm_project.id}")
            await projects.init_prefect_concurrency(orm_project.id)
        session.commit()
        logger.info(f"Created project {orm_project.id}")
        session.refresh(orm_project)
        return schemas.ProjectResponse.model_validate(orm_project)
    except Exception as e:
        session.rollback()
        raise e


def update_project(
    session: sqlalchemy.orm.Session, project_id: int, project: schemas.ProjectUpdate
) -> schemas.ProjectDetail | None:
    """Update an project."""
    orm_project = get_non_deleted_project_model(session, project_id)
    if orm_project is None:
        return None
    for field, value in project.dict(exclude_unset=True).items():
        setattr(orm_project, field, value)
    session.commit()
    session.refresh(orm_project)
    return schemas.ProjectDetail.model_validate(orm_project)


def get_project(session: sqlalchemy.orm.Session, project_id: int) -> schemas.ProjectDetail | None:
    """Get an project."""
    orm_project = get_non_deleted_project_model(session, project_id)
    if orm_project is None:
        return None
    return schemas.ProjectDetail.model_validate(orm_project)


def get_all_projects(
    session: sqlalchemy.orm.Session, start: int = 0, end: int = 100
) -> list[schemas.ProjectListResponse]:
    """Get projects."""
    query = (
        sqlalchemy.select(models.Project)
        .filter(models.Project.deleted_at.is_(None))
        .order_by(models.Project.id.desc())
        .offset(start)
        .limit(end)
    )
    projects = session.scalars(query).all()
    if not projects:
        return []
    return [schemas.ProjectListResponse.model_validate(project) for project in projects]


def get_user_projects(
    session: sqlalchemy.orm.Session, user_id: int, start: int = 0, end: int = 100
) -> list[schemas.ProjectListResponse]:
    """Get projects for a user."""
    # To be implemented
    query = (
        session.query(models.Project)
        .join(user_project_associations.UserProjectAssociations)
        .filter(user_project_associations.UserProjectAssociations.user_id == user_id)
        .order_by(models.Project.id.desc())
        .offset(start)
        .limit(end)
    )
    projects = query.all()
    if not projects:
        return []
    return [schemas.ProjectListResponse.model_validate(project) for project in projects]


def get_orm_project_with_guard(session: sqlalchemy.orm.Session, project_id: int) -> None:
    """Guard for null instnaces."""
    orm_project = get_non_deleted_project_model(session, project_id)
    if orm_project is None:
        raise exceptions.ProjectNotFound()


def get_non_deleted_project_model(
    session: sqlalchemy.orm.Session, project_id: int
) -> models.Project | None:
    """Get a non-deleted project model."""
    orm_project = (
        session.query(models.Project)
        .filter(
            models.Project.deleted_at.is_(None),
            models.Project.id == project_id,
        )
        .first()
    )
    return orm_project


def delete_project(
    session: sqlalchemy.orm.Session, project_id: int, delete_project_db: bool = False
) -> None:
    """Delete an project."""
    orm_project = get_non_deleted_project_model(session, project_id)
    if orm_project is None:
        raise exceptions.ProjectNotFound()
    if delete_project_db and not config.settings.USE_MOCK_BQ:
        project_namespace = utils.get_project_namespace(project_id)
        projects.delete_project_db(project_namespace)
    orm_project.deleted_at = datetime.datetime.utcnow()
    session.add(orm_project)
    session.commit()
    return None
