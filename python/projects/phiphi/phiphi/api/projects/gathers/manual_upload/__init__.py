"""Init."""
from phiphi.api.projects.gathers.manual_upload import (  # noqa: F401
    file_processing,
    routes,
    schemas,
)
