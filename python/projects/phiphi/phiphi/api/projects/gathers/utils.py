"""Utils for the gather."""
from typing import Any, Callable

import pydantic

from phiphi import config
from phiphi.api.projects.gathers import schemas as gather_schemas
from phiphi.api.projects.job_runs import schemas as job_runs_schemas


def validate_both_not_set(attribute_2: str) -> Callable[[Any, Any, pydantic.ValidationInfo], Any]:
    """Utility function to validate both attributes are not set.

    For example, if attribute_1 is set, the attribute_2 should be None.

    This would be added to a pydantic model like this:
    ```
    _validate_dependency_<attribute_1> = pydantic.field_validator(
        "<attribute_1>"
    )(utils.validate_dependency("<attribute_2>"))
    ```

    Args:
        attribute_2 (str): The attribute that should be None.
    """

    def fn(cls: Any, v: Any, info: pydantic.ValidationInfo) -> Any:
        if v is not None and info.data[attribute_2] is not None:
            raise ValueError(
                f"{info.field_name} can only be set if {attribute_2} is not set (None)"
            )

        return v

    return fn


def calculate_job_run_resource_estimate_from_result_count(
    max_gather_result_count: int, mean_cost_per_100k_results: float | None
) -> job_runs_schemas.JobRunResourceEstimate:
    """Compute the job run resource estimate using result count and expected cost per 100k results.

    Used as a generic way to estimate the cost of a gather based on the number of results.

    If no mean_cost_per_100k_results is provided, the default estimate value is used.

    Args:
        max_gather_result_count: The maximum number of results expected from the gather.
        mean_cost_per_100k_results: The expected cost per 100k results.

    Returns:
        JobRunResourceEstimate: The resource estimate.
    """
    if mean_cost_per_100k_results is not None:
        max_total_cost = max_gather_result_count * mean_cost_per_100k_results / 100_000
    else:
        max_total_cost = job_runs_schemas.ESTIMATE_DEFAULT

    return job_runs_schemas.JobRunResourceEstimate(
        max_total_cost=max_total_cost,
        max_gather_result_count=max_gather_result_count,
    )


def get_mean_cost_per_100k_results(
    child_type: gather_schemas.ChildTypeName,
) -> float | None:
    """Get the mean cost per 100k results for child type.

    Args:
        child_type (gather_schemas.ChildTypeName): The child type.

    Returns:
        float | None: The mean cost per 100k results or None if not found.
    """
    return config.settings.MEAN_COST_PER_100K_RESULTS_DICT.get(child_type.value, None)
