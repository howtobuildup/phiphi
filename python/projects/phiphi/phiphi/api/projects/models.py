"""Project Models."""
import datetime
from typing import Optional

from sqlalchemy import ForeignKey, Index, func, orm

from phiphi import platform_db
from phiphi.api import base_models
from phiphi.api.projects import credit_allocations
from phiphi.api.projects.job_runs import models as job_run_models


class ProjectBase(platform_db.Base):
    """Project Model."""

    __abstract__ = True

    id: orm.Mapped[int] = orm.mapped_column(primary_key=True)
    name: orm.Mapped[str]
    description: orm.Mapped[str]
    workspace_slug: orm.Mapped[str] = orm.mapped_column(
        ForeignKey("workspaces.slug"), default="main"
    )
    pi_deleted_after_days: orm.Mapped[int]
    delete_after_days: orm.Mapped[int]
    expected_usage: orm.Mapped[Optional[str]]
    deleted_at: orm.Mapped[Optional[datetime.datetime]]
    dashboard_id: orm.Mapped[Optional[int]]
    has_unlimited_credits: orm.Mapped[bool] = orm.mapped_column(default=False)
    # Needs to be optional or problems with the database and we thought that this was better then
    # using a server_default=sa.sql.expression.false() in the migration.
    checked_problem_statement: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)
    checked_sources: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)
    checked_gather: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)
    checked_classify: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)
    checked_visualise: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)
    checked_explore: orm.Mapped[Optional[bool]] = orm.mapped_column(default=False)


class Project(ProjectBase, base_models.TimestampModel):
    """Project model that can inherit from multiple models."""

    __tablename__ = "projects"
    __table_args__ = (
        # Requests with `WHERE project_id and deleted_at is not null` will also use this index.
        Index("idx_project_id_deteted_at", "id", "deleted_at"),
        Index(
            "idx_deleted_at",
            "deleted_at",
        ),
    )

    # Relationship to get all related JobRuns for project, ordered by id descending
    job_runs = orm.relationship(
        "JobRuns",
        order_by="desc(JobRuns.id)",
        primaryjoin="Project.id == foreign(JobRuns.project_id)",
        lazy="dynamic",
    )

    credit_allocations = orm.relationship(
        "CreditAllocations",
        order_by="desc(CreditAllocations.id)",
        primaryjoin="Project.id == foreign(CreditAllocations.project_id)",
        lazy="dynamic",
    )

    @property
    def latest_job_run(self) -> job_run_models.JobRuns | None:
        """Property to get the most recent JobRun."""
        latest_run: job_run_models.JobRuns | None = self.job_runs.first()

        if latest_run is None:
            return None

        return latest_run

    @property
    def last_job_run_completed_at(self) -> datetime.datetime | None:
        """Property to get the last job run completed at."""
        last_complete_job_run: job_run_models.JobRuns | None = self.job_runs.filter(
            job_run_models.JobRuns.completed_at.isnot(None),
        ).first()
        if last_complete_job_run is None:
            return None

        return last_complete_job_run.completed_at

    @property
    def total_costs(self) -> float:
        """Calculates the total costs of all related JobRuns."""
        total = (
            # Needed override the order_by or there is an error
            self.job_runs.order_by(None)
            .with_entities(func.sum(job_run_models.JobRuns.total_cost))
            .scalar()
        )
        return total or 0.0  # Return 0.0 if total is None

    @property
    def total_allocated_credits(self) -> float:
        """Calculates the total credited amount of all related CreditAllocations."""
        total = (
            # Needed override the order_by or there is an error
            self.credit_allocations.order_by(None)
            .with_entities(func.sum(credit_allocations.CreditAllocations.amount))
            .scalar()
        )
        return total or 0.0  # Return 0.0 if total is None

    @property
    def estimated_total_costs(self) -> float:
        """Calculates the estimated max total costs of all related JobRuns.

        This is the total cost + the estimated max cost of all job runs that are not yet completed.

        Returns:
            float: The estimated total costs.
        """
        # Estimated total cost of job runs that are not complete
        estimate_total_costs_of_imcomplete_jobs = (
            self.job_runs.filter(job_run_models.JobRuns.completed_at.is_(None))
            # Needed override the order_by or there is an error
            .order_by(None)
            .with_entities(func.sum(job_run_models.JobRuns.estimated_total_cost))
            .scalar()
        )
        return (estimate_total_costs_of_imcomplete_jobs or 0.0) + self.total_costs
