"""Script to initialize Prefect environment."""
import argparse
import asyncio
import logging
import re

# No stubs for prefect_gcp
import prefect_gcp  # type: ignore[import-untyped]

from phiphi import config, constants, prefect_deployments, utils

utils.init_logging()
utils.init_sentry()

logger = logging.getLogger(__name__)


async def init_storage_block() -> None:
    """Initialize the storage block by setting up a GCS bucket if specified in the configuration.

    Expects PREFECT_DEFAULT_RESULT_STORAGE_BLOCK in the format 'gcs-bucket/<bucket_name>'.
    """
    storage_block = config.settings.PREFECT_DEFAULT_RESULT_STORAGE_BLOCK
    logger.info(f"Storage block configuration: {storage_block}")

    if config.settings.CREATE_PREFECT_STORAGE_BLOCK_ON_INIT and storage_block:
        if storage_block.startswith("gcs-bucket/"):
            logger.info("Initializing GCS storage bucket...")

            # Extract the bucket name using regex
            match = re.match(r"^gcs-bucket/([^/]+)$", storage_block)
            if not match:
                raise ValueError("Invalid GCS bucket format in storage block configuration.")

            bucket_name = match.group(1)
            logger.info(f"Extracted bucket name: {bucket_name}")

            # Initialize the GCS bucket
            gcs_storage = prefect_gcp.GcsBucket(bucket=bucket_name)
            await gcs_storage.save(name=bucket_name, overwrite=True)
            logger.info(f"GCS bucket '{bucket_name}' initialized and saved.")
        else:
            raise ValueError("Unknown storage block format. Only 'gcs-bucket' is supported.")


async def main(image: str) -> None:
    """Initialize Prefect environment."""
    await init_storage_block()
    logger.info("Prefect initialization complete.")
    await prefect_deployments.create_all_deployments(image=image)
    logger.info("Prefect deployments created.")


if __name__ == "__main__":
    logger.info("Running Prefect init...")
    parser = argparse.ArgumentParser(
        description="Init a prefect environment for PhiPhi", prog="PhiPhiPrefectInit"
    )
    parser.add_argument(
        "--image",
        default=constants.DEFAULT_IMAGE,
        help=("The image which will be ran when any of the Prefect deployments are run."),
    )
    args = parser.parse_args()

    asyncio.run(
        main(
            image=args.image,
        )
    )
