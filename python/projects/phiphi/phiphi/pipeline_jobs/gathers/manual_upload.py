"""Manual upload pipeline gathers."""
from datetime import datetime

import pandas as pd
import pandera as pa

from phiphi.api.projects.gathers import schemas
from phiphi.api.projects.gathers.manual_upload import schemas as manual_upload_schemas
from phiphi.pipeline_jobs import constants, gather_batches
from phiphi.pipeline_jobs import utils as pipeline_jobs_utils
from phiphi.pipeline_jobs.gathers import types as gather_types

manual_upload_schema = pa.DataFrameSchema(
    {
        "platform": pa.Column(
            pa.String,
            checks=pa.Check.isin([e.value for e in schemas.Platform]),
            nullable=False,
        ),
        "data_type": pa.Column(
            pa.String, checks=pa.Check.isin([e.value for e in schemas.DataType]), nullable=False
        ),
        "message_id_pi": pa.Column(pa.String, nullable=False, coerce=True),
        "message_author_id_pi": pa.Column(pa.String, nullable=False, coerce=True),
        "message_author_name_pi": pa.Column(pa.String, nullable=False, coerce=True),
        # For comments this is the post that the comment is on OR the comment that the reply is on.
        "comment_replied_to_id_pi": pa.Column(pa.String, nullable=True, coerce=True),
        # For comments this is the root post for that comment. For posts this is None.
        "comment_parent_post_id_pi": pa.Column(pa.String, nullable=True, coerce=True),
        "message_text_pi": pa.Column(pa.String, nullable=False),
        "message_url_pi": pa.Column(pa.String, nullable=False, coerce=True),
        "message_datetime": pa.Column(
            pa.engines.pandas_engine.DateTime(  # type: ignore[call-arg]
                unit="ms",
                tz="UTC",
                to_datetime_kwargs={"utc": True, "format": "ISO8601"},
            ),
            coerce=True,
            nullable=False,
        ),
        # Stats
        # These are not nullable as should be set to 0 if not present.
        "like_count": pa.Column(pa.Int, nullable=False, default=0, coerce=True),
        "share_count": pa.Column(pa.Int, nullable=False, default=0, coerce=True),
        "comment_count": pa.Column(pa.Int, nullable=False, default=0, coerce=True),
        "tiktok_post_plays": pa.Column(pa.Int64, nullable=True, default=None),
    },
    strict=True,
)


def to_json(
    manual_upload_df: pd.DataFrame,
) -> str:
    """Convert manual upload DataFrame to JSON.

    This is needed as to_json will convert datetimes to integers, this then allows for a more
    controlled conversion to JSON.

    Args:
        manual_upload_df: DataFrame containing manual upload data.

    Returns:
        JSON string.
    """
    formated_df = manual_upload_df.copy()

    formated_df["message_datetime"] = formated_df["message_datetime"].dt.strftime(
        "%Y-%m-%dT%H:%M:%S.%fZ"
    )
    return formated_df.to_json(orient="records")


def form_gather_batches(
    manual_upload_df: pd.DataFrame,
    gather_id: int,
    job_run_id: int,
    batch_offset: int,
    batch_size: int,
) -> pd.DataFrame:
    """Form gather batches from a manual upload DataFrame.

    Creating a valid gather batch DataFrame from a manual upload DataFrame with the
    correct batches. To be used by the manual upload pipeline.

    Args:
        manual_upload_df: DataFrame containing manual upload data.
        gather_id: Gather ID.
        job_run_id: Job run ID.
        batch_offset: Batch offset. The batch id to start from so that it can be used with a
            batch of batches.
        batch_size: Batch size.

    Returns:
        DataFrame containing gather batches.
    """
    if "tiktok_post_plays" in manual_upload_df.columns:
        manual_upload_df["tiktok_post_plays"] = manual_upload_df["tiktok_post_plays"].astype(
            "Int64"
        )
    # validate the manual_upload_df
    validated_df = manual_upload_schema.validate(manual_upload_df)
    batches_processed = 0
    base_data = {
        "gather_id": gather_id,
        "job_run_id": job_run_id,
        "gather_type": schemas.ChildTypeName.manual_upload.value,
        "batch_id": batch_offset,  # This will be updated for each batch
        "last_processed_at": pd.NaT,  # Use pd.NaT for missing datetime values
    }
    gather_batches_list: list[dict] = []
    for i in range(0, validated_df.shape[0], batch_size):
        new_row = base_data.copy()
        new_row["json_data"] = to_json(validated_df.iloc[i : i + batch_size])
        new_row["batch_id"] = batch_offset + batches_processed
        new_row["gathered_at"] = datetime.utcnow()
        gather_batches_list.append(new_row)

        batches_processed += 1

    gather_batches_df = pd.DataFrame(gather_batches_list)
    validated_gather_batches_df = gather_batches.gather_batches_schema.validate(gather_batches_df)
    return validated_gather_batches_df


def write_manual_upload_to_gather_batches(
    manual_upload_gather: manual_upload_schemas.ManualUploadGatherResponse,
    job_run_id: int,
    bigquery_dataset: str,
    bigquery_table: str = constants.GATHER_BATCHES_TABLE_NAME,
    batch_size: int = constants.DEFAULT_BATCH_SIZE,
    batch_of_batches_size: int = constants.DEFAULT_BATCH_OF_BATCHES_SIZE,
) -> gather_types.ScrapeResponse:
    """Write manual upload data to gather batches.

    This function reads the manual_uploaded data in chunks and writes it to gather batches.
    Allowing for the processing of batches of batches.

    Args:
        manual_upload_gather: Manual upload gather response.
        job_run_id: Job run ID.
        bigquery_dataset: BigQuery dataset.
        bigquery_table: Big query table name. Defaults to PhiPhi default gather batches table.
        batch_size: Batch size. Defaults to PhiPhi default batch size.
        batch_of_batches_size: Batch of batches size. Defaults to PhiPhi default batch of batches
            size.

    Returns:
        Scrape response.
    """
    total_rows = 0
    total_batches = 0
    read_chunk_size = batch_size * batch_of_batches_size
    # Iterate through CSV in batches
    # read_csv support gs and s3 paths
    # https://pandas.pydata.org/docs/reference/api/pandas.read_csv.html
    for df_batch in pd.read_csv(manual_upload_gather.file_url, chunksize=read_chunk_size):
        total_rows += len(df_batch)
        gather_batches_df = form_gather_batches(
            manual_upload_df=df_batch,
            gather_id=manual_upload_gather.id,
            job_run_id=job_run_id,
            batch_offset=total_batches,
            batch_size=batch_size,
        )
        pipeline_jobs_utils.write_data(gather_batches_df, bigquery_dataset, bigquery_table)

        total_batches += len(gather_batches_df)

    return gather_types.ScrapeResponse(
        total_items=total_rows, total_batches=total_batches, total_cost=0
    )
